var app={
  inicio: function(){
    // Definición de valores iniciales
    DIAMETRO_BOLA = 50;
    dificultad = 0;
    velocidadX = 0;
    velocidadY = 0;
    puntuacion = 0;

    bolasEnAgujero = 0;
    
    alto  = document.documentElement.clientHeight;
    ancho = document.documentElement.clientWidth;
    
    app.vigilaSensores();
    app.iniciaJuego();
  },

  iniciaJuego: function(){

    function preload() {
      game.physics.startSystem(Phaser.Physics.ARCADE);

      // Establece color de fondo
      game.stage.backgroundColor = '#d0d0d0'; //'#f27d0c';
      
      // Carga  imágenes
      game.load.image('objetivo', 'assets/objetivo.png');
      
      game.load.image('bloque', 'assets/bloque.png');

      game.load.image('agujeroA', 'assets/agujeroA.png');
      game.load.image('agujeroR', 'assets/agujeroR.png');
      game.load.image('agujeroV', 'assets/agujeroV.png');

      game.load.image('bolaA', 'assets/bolaA.png');
      game.load.image('bolaR', 'assets/bolaR.png');
      game.load.image('bolaV', 'assets/bolaV.png');

      
      // Carga audios
      game.load.audio('bubble', 'assets/bubble_pop-15348_33253-lq.mp3');
      game.load.audio('mp3Agujero', 'assets/agujero-167338_3062051-lq.mp3');
      
    }

    function create() {
      // Crea texto para puntuación
      scoreText = game.add.text(16, 16, puntuacion, { fontSize: '50px', fill: '#757676' });
      finalText = game.add.text(10, alto/2, "", { fontSize: '30px', fill: '#E00' });
      
      // Crea sprites con posición aleatoria
      objetivo = game.add.sprite(app.inicioX(), app.inicioY(), 'objetivo');
      
      bloque = game.add.sprite(app.inicioX(), app.inicioY(), 'bloque');

      agujeroA = game.add.sprite(app.inicioX(), app.inicioY(), 'agujeroA');
      agujeroR = game.add.sprite(app.inicioX(), app.inicioY(), 'agujeroR');
      agujeroV = game.add.sprite(app.inicioX(), app.inicioY(), 'agujeroV');

      bolaA = game.add.sprite(app.inicioX(), app.inicioY(), 'bolaA');
      bolaR = game.add.sprite(app.inicioX(), app.inicioY(), 'bolaR');
      bolaV = game.add.sprite(app.inicioX(), app.inicioY(), 'bolaV');

      
      // Crea cuerpo de características físicas a cada objeto
      game.physics.arcade.enable(bolaA);
      game.physics.arcade.enable(bolaV);
      game.physics.arcade.enable(bolaR);

      game.physics.arcade.enable(agujeroA);
      game.physics.arcade.enable(agujeroV);
      game.physics.arcade.enable(agujeroR);

      game.physics.arcade.enable(objetivo);
      game.physics.arcade.enable(bloque);

      // Colisón con bordes del World
      // Asigna el evento al colisionar con los bordes

      bolaA.body.collideWorldBounds = true;
      bolaA.body.onWorldBounds = new Phaser.Signal();
      bolaA.body.onWorldBounds.add(app.decrementaPuntuacion, this);

      bolaR.body.collideWorldBounds = true;
      bolaR.body.onWorldBounds = new Phaser.Signal();
      bolaR.body.onWorldBounds.add(app.decrementaPuntuacion, this);      

      bolaV.body.collideWorldBounds = true;
      bolaV.body.onWorldBounds = new Phaser.Signal();
      bolaV.body.onWorldBounds.add(app.decrementaPuntuacion, this);      

      
      // Características físicas
      // Bounce: rebote 1=rebote total; 0.5 = 50% velocidad rebote
      // Gravity: gavedad aplicada al elemento

      bolaA.body.bounce.set(0.8);
      bolaA.body.gravity.set(180,180);
      bolaR.body.bounce.set(0.8);
      bolaR.body.gravity.set(180,180);
      bolaV.body.bounce.set(0.8);
      bolaV.body.gravity.set(180,180);

      //game.physics.arcade.gravity.y = 100; // Pone gravedad a todo
      
      objetivo.body.collideWorldBounds = true;
      objetivo.body.bounce.set(0.8); // con set se pone en todas direcciones. La otra opción es .x = o .y =

      bloque.body.collideWorldBounds = true;
      bloque.body.bounce.set(0.8);
      

      // Adición del audio
      fx = game.add.audio('bubble');
      fx.allowMultiple = false;
      fx1 = game.add.audio('mp3Agujero');
      fx1.allowMultiple = false;      
      
      
    }

    function update(){
      var factorDificultad = (100 + (dificultad * 100));
      

      bolaA.body.velocity.y = (velocidadY * factorDificultad);
      bolaA.body.velocity.x = (velocidadX * (-1 * factorDificultad));
      
      bolaR.body.velocity.y = (velocidadY * factorDificultad);
      bolaR.body.velocity.x = (velocidadX * (-1 * factorDificultad));

      bolaV.body.velocity.y = (velocidadY * factorDificultad);
      bolaV.body.velocity.x = (velocidadX * (-1 * factorDificultad));
      
      
      game.physics.arcade.overlap(bolaA, objetivo, app.incrementaPuntuacion, null, this);
      game.physics.arcade.overlap(bolaR, objetivo, app.incrementaPuntuacion, null, this);
      game.physics.arcade.overlap(bolaV, objetivo, app.incrementaPuntuacion, null, this);
      
      //game.physics.arcade.collide([bolaA, bolaR, bolaV], [bolaA, bolaR, bolaV], null, null, this);
      game.physics.arcade.collide([bolaA, bolaR, bolaV], bloque, null, null, this);
      game.physics.arcade.overlap(bolaA, agujeroA, app.entraAgujeroA, null, this);
      game.physics.arcade.overlap(bolaR, agujeroR, app.entraAgujeroR, null, this);
      game.physics.arcade.overlap(bolaV, agujeroV, app.entraAgujeroV, null, this);


    }

    var estados = { preload: preload, create: create, update: update };
    var game = new Phaser.Game(ancho, alto, Phaser.CANVAS, 'phaser',estados);
  },

  decrementaPuntuacion: function(){
    puntuacion = puntuacion-1;
    scoreText.text = puntuacion;
  },

  incrementaPuntuacion: function(){

    puntuacion = puntuacion+5;
    scoreText.text = puntuacion;

    objetivo.body.x = app.inicioX();
    objetivo.body.y = app.inicioY();

    if (puntuacion > 0){
      dificultad = dificultad + 1;
    }

    fx.play();
  },
  
  tocaBloque: function(){
    fx.play();
  },
  
  entraAgujero: function(bolaSeleccionada){
    /*
    bolaSeleccionada.x = 0;
    bolaSeleccionada.y = alto - DIAMETRO_BOLA;
    bolaSeleccionada.body.moves = false;
    bolaSeleccionada.scale.setTo(0.5,0.5);
    */
    fx1.play();
    
  },
  entraAgujeroA: function(){

    bolaA.x = bolasEnAgujero * 30;
    bolaA.y = alto - DIAMETRO_BOLA;
    bolaA.body.moves = false;
    bolaA.scale.setTo(0.5,0.5);

    fx1.play();
    bolasEnAgujero++;
    if (bolasEnAgujero == 3) app.finalJuego();
  },
  entraAgujeroR: function(){

    bolaR.x = bolasEnAgujero * 30;
    bolaR.y = alto - DIAMETRO_BOLA;
    bolaR.body.moves = false;
    bolaR.scale.setTo(0.5,0.5);

    fx1.play();
    bolasEnAgujero++;
    if (bolasEnAgujero == 3) app.finalJuego();
  },
  entraAgujeroV: function(){

    bolaV.x = bolasEnAgujero * 30;
    bolaV.y = alto - DIAMETRO_BOLA;
    bolaV.body.moves = false;
    bolaV.scale.setTo(0.5,0.5);

    fx1.play();
    bolasEnAgujero++;
    if (bolasEnAgujero == 3) app.finalJuego();
  },

  finalJuego: function(){
    finalText.text = "¡FIN DEL JUEGO!\n(Agitar para empezar)";
  },


  inicioX: function(){
    return app.numeroAleatorioHasta(ancho - DIAMETRO_BOLA );
  },

  inicioY: function(){
    return app.numeroAleatorioHasta(alto - DIAMETRO_BOLA );
  },

  numeroAleatorioHasta: function(limite){
    return Math.floor(Math.random() * limite);
  },

  vigilaSensores: function(){

    function onError() {
      console.log('onError!');
    }

    function onSuccess(datosAceleracion){
      app.detectaAgitacion(datosAceleracion);
      app.registraDireccion(datosAceleracion);
    }

    navigator.accelerometer.watchAcceleration(onSuccess, onError,{ frequency: 10 });
  },

  detectaAgitacion: function(datosAceleracion){
    var agitacionX = datosAceleracion.x > 10;
    var agitacionY = datosAceleracion.y > 10;

    if (agitacionX || agitacionY){
      setTimeout(app.recomienza, 1000);
    }
  },

  recomienza: function(){
    document.location.reload(true);
  },

  registraDireccion: function(datosAceleracion){
    velocidadX = datosAceleracion.x ;
    velocidadY = datosAceleracion.y ;
  }

};

if ('addEventListener' in document) {
  document.addEventListener('deviceready', function() {
    app.inicio();
  }, false);
}